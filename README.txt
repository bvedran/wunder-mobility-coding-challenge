WunderFleet Coding Challenge Readme

Prior to this challenge I had no experience with Yii2 framework. Still, I wanted to use it for the challenge because it was bonus point for position I've applied to.

As a basis I used basic Yii skeleton app which I installed via composer. Later I added couple of plugins.

It is enough to do composer install in application directory and run the app via internal server as ./yii serve. It will load server on localhost:8080 by default.

My solution also uses MySQL, as such MySQL server is another dependency. I've included a schema of the database.

I have developed two forms as a solution of the project.

First one is I did following mostly tutorials for Yii2, where I found about magic of ActiveRecord and ActiveForm.

Due to the need to be able to resume the form on page reload or navigating back to the form, I stored values in session. 

This is not ideal solution, from performance aspect it would be better to have temp table in database to store the data until form is completed. 

Obviously, even then we would need to store an identifier in either session/cookie/localstorage.

Second one was actually when I found out about FormWizzard plugin, it felt like a cheating! And especially since I already had implemented a call to external service.

Also, I've written basic unit tests for methods implemented in Users class (for all but one), you can simply run them by executing: ./vendor/bin/codecept run once positioned in application directory.

Over the couple days I've worked with it, I can say I very much like Yii2. It is miles ahead Zend I'm using at work.

Finally, to answer the questions from the assignment:

1. Describe possible performance optimizations for your Code.
A: The most obvious performance optimization is to use temporary db table instead of session for persistance.

2. Which things could be done better, than you�ve done it?
A: First form lacks button to go back a step, there is only a "Next" button. Most of the fields in the form are just strings and field types in the database could be better. I just kept it simple for now.
Validation is too simple, mostly lacking any format check, you can enter numbers for first/last name, etc. Design is basic.