<?php

namespace tests\unit\models;

use app\models\Users;
use Yii;

class UsersTest extends \Codeception\Test\Unit
{
    public $fieldName = 'first_name';
    public $value = 'vedran';

    public function testTableName()
    {
        $expectedResult = 'users';
        $this->assertEquals($expectedResult, Users::tableName());
    }

    public function testGetCurrentStep() {
        $this->assertEquals(1, Users::getCurrentStep());
    }

    public function testSetToSession() {
        $Users = new Users();
        $this->assertEquals(true, $Users->setToSession($this->fieldName, $this->value));
        $this->assertEquals($this->value, Yii::$app->session->get($this->fieldName));
    }

    public function testGetFromSession() {
        $Users = new Users();
        $this->assertEquals(true, $Users->setToSession($this->fieldName, $this->value));
        $this->assertEquals($this->value, $Users->getFromSession($this->fieldName));
    }

    public function testUpdateModelFields() {
        $Users = new Users();
        $this->assertEquals(true, $Users->setToSession($this->fieldName, $this->value));
        $this->assertEquals(true, $Users->updateModelFields());
        $this->assertEquals($this->value, $Users->first_name);
    }

    public function testUpdateSession() {
        $Users = new Users();
        $Users->first_name = 'vedran';
        $this->assertEquals(true, $Users->updateSession());
        $this->assertEquals($this->value, $Users->getFromSession($this->fieldName));
    }

    public function testPostToExternalEndpoint() {
        $Users = new Users();
        $Users->customerId = 1;
        $Users->IBAN = 'IBAN';
        $Users->account_owner = "vedranbakic";
        $this->assertFalse($Users->postToExternalEndpoint());
        $this->assertNotEquals(false, $Users->postToExternalEndpoint('https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data'));
    }
    

}
