<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form ActiveForm */
?>
<div class="users">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'first_name') ?>
        <?= $form->field($model, 'last_name') ?>
        <?= $form->field($model, 'phone') ?>
        <?= $form->field($model, 'zipcode') ?>
        <?= $form->field($model, 'street') ?>
        <?= $form->field($model, 'city') ?>
        <?= $form->field($model, 'account_owner') ?>
        <?= $form->field($model, 'house_number') ?>
        <?= $form->field($model, 'IBAN') ?>
        <?= $form->field($model, 'paymentDataId') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- users -->
