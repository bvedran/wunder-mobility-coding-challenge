<?php

use yii\helpers\Html;

$this->title = 'Thank you for your submission!';

?>
<div class="users-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p><a class="btn btn-success" href="/">Go Back to Home</a></p>
</div>
