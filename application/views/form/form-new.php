<div><a class="btn btn-warning" style="margin-bottom: 15px;" href="/">Cancel</a></div>

<h1>Registration Form</h1>

<?php

use buttflattery\formwizard\FormWizard;
echo FormWizard::widget([
    'enablePersistence' => true,
    'steps'=>[
        [
            'model'=>$model,
            'title'=>'Basic info',
            'description'=>'',
            'formInfoText'=>'',
            'fieldConfig'=>[
                'only' => ['first_name', 'last_name', 'phone']                
            ]
        ],
        [
            'model'=> $model,
            'title'=>'Address info',
            'description'=>'',
            'formInfoText'=>'',
            'fieldConfig'=>[
                'only' => ['street', 'house_number', 'zipcode', 'city']                
            ]
        ],
        [
            'model'=> $model,
            'title'=>'Payment info',
            'description'=>'',
            'formInfoText'=>'',
            'fieldConfig'=>[
                'only' => ['account_owner', 'IBAN']                
            ]
        ],
    ]
]);

?>



