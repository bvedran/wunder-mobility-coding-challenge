<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Registration Form - Basic info';

Yii::$app->session->set('step', 1);

?>

<div><a class="btn btn-warning" href="/">Cancel</a></div>

<h1><?= Html::encode($this->title) ?></h1>

<div class="users">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'first_name') ?>
        <?= $form->field($model, 'last_name') ?>
        <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '999-999-9999',
        ]) ?>
        

        <div class="form-group">
            <?= Html::submitButton('Next', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div>


