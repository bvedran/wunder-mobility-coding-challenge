<div class="users-index">

    <?php if(Yii::$app->session->hasFlash('message')):?>
    <div class="info">
        <?php echo Yii::$app->session->getFlash('message'); ?>
    </div>
    <?php endif; ?>

</div>

<p><a class="btn btn-warning" href="/">Go Back to Home</a></p>