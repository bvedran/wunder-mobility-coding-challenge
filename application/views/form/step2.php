<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Registration Form - Address info';

Yii::$app->session->set('step', 2);

?>

<div><a class="btn btn-warning" href="/">Cancel</a></div>

<h1><?= Html::encode($this->title) ?></h1>

<div class="users">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'street') ?>
        <?= $form->field($model, 'house_number') ?>
        <?= $form->field($model, 'zipcode') ?>
        <?= $form->field($model, 'city') ?>
        

        <div class="form-group">
            <?= Html::submitButton('Next', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div>

