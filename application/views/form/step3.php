<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Registration Form - Payment info';

Yii::$app->session->set('step', 3);

?>

<div><a class="btn btn-warning" href="/">Cancel</a></div>

<h1><?= Html::encode($this->title) ?></h1>

<div class="users">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'account_owner') ?>
        <?= $form->field($model, 'IBAN') ?>

        <div class="form-group">
            <?= Html::submitButton('Next', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div>
