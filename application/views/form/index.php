<?php
$this->title = 'Home';
?>

<h1>Home</h1>

<p>Please fill out our form.</p>

<a class="btn btn-success" href="/form/form">Start Form</a>
<p>Brief description:</p>
<ul>
    <li>Form developed via Yii ActiveForm class. Uses ActiveRecord for communication with db.</li>
    <li>Heavily relies on session for temporary data.</li>
    <li>Reload of the page will bring out the Confirmation dialog to re-submit the data since it uses POST from step to step.</li>
</ul>

<a class="btn btn-success" href="/form/form-new">Start Form New</a>
<p>Brief description:</p>
<ul>
    <li>Form developed via Yii FormWizard plugin. It uses ActiveRecord and ActiveForm in the background.</li>
    <li>Form so simple that it looks like cheating.</li>
    <li>Relies on browser local storage for temp data, when persistence is enabled.</li>
    <li>Requires "Restore" click to load the previously submitted data. I found out that it ain't 100% reliable.</li>
    <li>Apart from "Next" button, there is "Previous" button as well - part of the plugin.</li>    
</ul>

<p>Same for both forms:</p>
<ul>
    <li>Both forms save data to local MySQL database, post to external endpoint and then update MySQL table with additional info obtained from service.</li>
    <li>Acts as a single page app - there is no feeling of redirect from step to step.</li>
</ul>