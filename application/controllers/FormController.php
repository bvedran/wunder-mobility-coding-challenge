<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class FormController extends Controller
{

    /**
     * Homepage action. Only has buttons for loading forms.
     */
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionForm() {
        $model = new Users();

        //load current step from session
        $currentStep = $model->getCurrentStep();
        $model->scenario = 'step' . ($currentStep);

        $model->updateModelFields();
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if($currentStep == 3) { //in case submit came from final step
                Yii::$app->session->destroy(); //clear the session, we won't be resuming the form
                if ($model->save()) {
                    return $this->redirect(['update-payment-id', 'customerId' => $model->customerId]);
                } else {
                    //Yii::$app->session->setFlash('error', "Unfortunately, there was an error with your application. Please try again soon.");
                    $this->redirect('error');
                }
            } else {
                //in case final step not reached yet
                $currentStep++;
                $model->updateSession();
            }            
        }

        return $this->render('step' . $currentStep, [
            'model' => $model,
        ]);
    }

    public function actionFormNew() {
        $model = new Users();
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                //we redirect to the next action that should post to external service and update model in db
                return $this->redirect(['update-payment-id', 'customerId' => $model->customerId]);
            } else {
                $this->redirect('error');
            }
        }

        return $this->render('form-new', [
            'model' => $model,
        ]);
    }

    public function actionUpdatePaymentId() {

        //fetch model from db based on id
        $customer = Users::findOne(Yii::$app->getRequest()->getQueryParam('customerId'));

        //call method that posts to external service, passing the url of the service (obtained from config)
        $paymentdataID = $customer->postToExternalEndpoint(Yii::$app->params['wunderfleet_api']);

        if($customer->savePaymentData($paymentdataID)) {
            Yii::$app->session->setFlash('success', "User created successfully.");
            Yii::$app->session->setFlash('info', "Your payment id is: " . $customer->paymentDataId);
            $this->redirect('thank-you');
        } else {
            $this->redirect('error');
        }
        
    }

    public function actionThankYou() {
        return $this->render('thank-you');
    }

    public function actionError() {
        Yii::$app->session->destroy();
        Yii::$app->session->setFlash('error', "Unfortunately, there was an error with your application. Please try again soon.");
        return $this->render('error');
    }
    
}
