<?php

namespace app\models;

use Yii;
use yii\httpclient\Client;

/**
 * This is the model class for table "users".
 *
 * @property int $customerId
 * @property string $first_name
 * @property string $last_name
 * @property int $phone
 * @property string $street
 * @property string $house_number
 * @property int $zipcode
 * @property string $city
 * @property string $account_owner
 * @property string $IBAN
 */
class Users extends \yii\db\ActiveRecord
{
    //fields to escape as we won't store them in session
    public $excludedFields = array('customerId', 'paymentDataId');
    const SCENARIO_STEP1 = 'step1';
    const SCENARIO_STEP2 = 'step2';
    const SCENARIO_STEP3 = 'step3';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    public function scenarios()
    {
        //create custom scenarios for validation on step1/step2/step3
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_STEP1] = ['first_name', 'last_name', 'phone'];
        $scenarios[self::SCENARIO_STEP2] = ['street', 'house_number', 'zipcode', 'city'];
        $scenarios[self::SCENARIO_STEP3] = ['account_owner', 'IBAN'];
        return $scenarios;
    }

    
    public static function getCurrentStep() {
        if(Yii::$app->session->get('step')) {
            return Yii::$app->session->get('step');
        }
        return 1;
    }

    public function setToSession($fieldName, $value) {
        //basic method to encapsulate setting value to session
        if(!in_array($fieldName, $this->excludedFields)) {
            try {
                Yii::$app->session->set($fieldName, $value);
            } catch (\Throwable $th) {
                return false;
            }
        }
        return true;
    }

    public function getFromSession($fieldName) {
        //basic method to encapsulate getting value from session
        if(Yii::$app->session->has($fieldName) && !in_array($fieldName, $this->excludedFields)) {
            return Yii::$app->session->get($fieldName);
        }
        return false; //only in case nothing got returned from session
    }

    public function updateModelFields() {
        //iterate through properties of this model and update it with the values from session
        foreach ($this->attributes as $fieldName => $value) {
            $this->$fieldName = $this->getFromSession($fieldName);
        }
        return true;
    }

    public function updateSession() {
        //iterate through properties of this model and update session with current model values
        foreach ($this->attributes as $fieldName => $value) {
            $this->setToSession($fieldName, $value);
        }
        return true;
    }

    public function postToExternalEndpoint($url=false) {
        if(!$url) { //if no url passed
            return false;
        }
        //instatiating Client class and setting up for the call
        $httpClient = new Client();
        $request = $httpClient->createRequest()
            ->setUrl($url)
            ->setMethod('POST')
            ->setFormat(Client::FORMAT_JSON)
            ->setData(['customerId' => $this->customerId, 
                       'iban' => $this->IBAN, 
                       'owner' => $this->account_owner])
            ->send();

        if($request->getIsOk()) { //we check for 2xx response, if OK, return the value
            return $request->getData()['paymentDataId'];
        } else {
            return false;
        }
    }

    public function savePaymentData($paymentDataId) {
        if(!isset($paymentDataId)) {
            return false; //exit immediately if $paymentDataId is not set
        }
        $this->paymentDataId = $paymentDataId; //we update the model now
        if($this->validate() && $this->save()) {
            //everything fine, model in db updated with paymentDataId got from external service
            return true;
        }
        return false; //in case anything got wrong
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        //custom validation fields specified for certain scenarios
        //default is to validate everything
        return [
            [['first_name', 'last_name', 'phone'], 'required', 'on' => self::SCENARIO_STEP1],
            [['first_name', 'last_name', 'phone', 'street', 'house_number', 'zipcode', 'city'], 'required', 'on' => self::SCENARIO_STEP2],
            [['first_name', 'last_name', 'phone', 'street', 'house_number', 'zipcode', 'city', 'account_owner', 'IBAN'], 'required', 'on' => self::SCENARIO_STEP3],
            [['first_name', 'last_name', 'phone', 'street', 'house_number', 'zipcode', 'city', 'account_owner', 'IBAN'], 'required'],
            [['zipcode'], 'integer'],
            [['first_name', 'last_name', 'street', 'city', 'account_owner'], 'string', 'max' => 52],
            [['house_number', 'IBAN'], 'string', 'max' => 20],
            [['phone'], 'string', 'max' => 13]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'customerId' => 'Customer ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'phone' => 'Phone',
            'street' => 'Street',
            'house_number' => 'House Number',
            'zipcode' => 'Zipcode',
            'city' => 'City',
            'account_owner' => 'Account Owner',
            'IBAN' => 'IBAN',
        ];
    }
}
